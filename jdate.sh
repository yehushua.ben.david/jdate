#!/bin/bash
##
## Thanks to calj.net

m_hdn=0
m_day=0
m_month=0
m_year=0

NISSAN=1
IYAR=2
SIVAN=3
TAMUZ=4
AV=5
ELUL=6
TISHRI=7
CHESHVAN=8
KISLEV=9
TEVET=10
SHVAT=11
ADAR=12
ADAR1=12
ADAR2=13

RISHON=0
CHAMISHI=4
SHISHI=5
SHABAT=6
SHABBAT=6
CHASERA=1
SDURA=2
SHLEMA=3

setDate_int_int_int() {
  local day=$1
  local month=$2
  local year=$3
  local nextRH=0
  m_hdn=$(st_roshHashanaDay $year)
  nextRH=$(st_roshHashanaDay $((year + 1)))
  m_type=$((((nextRH - m_hdn) % 10) - 2))
  m_day=1
  m_month=$TISHRI
  m_year=$year
  if ((month > $(nbMonths))); then
    month=$(nbMonths)
  fi
  while ((m_month != month)); do
    m_hdn=$((m_hdn + $(getMonthLength)))

    if (((m_month + 1) > $(nbMonths))); then
      m_month=1
    else
      m_month=$((m_month + 1))
    fi
  done
  if ((day > $(getMonthLength))); then
    day=$(getMonthLength)
  fi
  m_day=$day
  m_hdn=$((m_hdn + (day - 1)))
  if ((m_hdn < 1)); then
    setHdn $m_hdn
  fi

}
getDay() { echo $m_day; }
getMonth() { echo $m_month; }
getYear() { echo $m_year; }
setDay() { setDate_int_int_int $1 $m_month $m_year; }
setMonth() { setDate_int_int_int $m_day $1 $m_year; }
setYear() { setDate_int_int_int $m_day $m_month $1; }
getDayOfWeek() { echo $((m_hdn % 7)); }

st_embolismicYear() {
  local year=$1
  echo $(((((12 * year) + 17) % 19) >= 12))
}

st_roshHashanaDay() {
  local hyear=$1

  local uday=$((24 * 1080))
  local Tsyn=$((29 * uday + 12 * 1080 + 793))
  local Ttohu=$((5 * 1080 + 204))
  local Tgatarad=$((9 * 1080 + 204))
  local Tzaken=$((18 * 1080))
  local Tbetutkafot=$((15 * 1080 + 589))
  local Nmonths=$(((235 * hyear - 234) / 19))
  local Tmolad=$((Nmonths * Tsyn + Ttohu))

  local days=$((Tmolad / uday))
  local parts=$((Tmolad - days * uday))
  local weekday=$(((1 + days) % 7))

  local adu=$((((weekday == 0) || (weekday == 3) || (weekday == 5))))
  local gatarad=$(($(st_embolismicYear hyear) && ((weekday == 2) && (parts >= Tgatarad))))
  if [ $hyear -eq 1 ]; then
    local betutkafot=0
  else
    local betutkafot=$(($(st_embolismicYear $((hyear - 1))) && ((weekday == 1) && (parts >= Tbetutkafot))))
  fi
  local zaken=0
  if (((adu == 0) && (gatarad == 0) && (betutkafot == 0))); then
    local zaken=$((parts >= Tzaken))
    if ((zaken)); then
      adu=$((((weekday == 2) || (weekday == 4) || (weekday == 6))))
    fi
  fi

  echo $((1 + days + adu + (2 * gatarad) + betutkafot + zaken))
}

isLeap() {
  st_embolismicYear $m_year
}

nbMonths() {
  echo $((12 + $(isLeap)))
}

getMonthLength() {

  if ((m_month == TISHRI || m_month == SIVAN || m_month == SHVAT || m_month == NISSAN || m_month == AV)); then
    echo 30
    return 
  fi
  if ((m_month == TEVET || m_month == IYAR || m_month == TAMUZ || m_month == ELUL)); then
    echo 29
    return 
  fi
  if ((m_month == ADAR1)); then
    if (($(isLeap))); then
      echo 30
    return 
    else
      echo 29
    return 
    fi
  fi

  if ((m_month == ADAR2)); then
    if (($(isLeap))); then
      echo 29
    return 
    else
      echo 0
    return 
    fi
  fi

  if ((m_month == CHESHVAN)); then
    if ((m_type == SHLEMA)); then
      echo 30
    return 
    else
      echo 29
    return 
    fi
  fi

  if ((m_month == KISLEV)); then
    if ((m_type == CHASERA)); then
      echo 29
    return 
    else
      echo 30
    return 
    fi
  fi
echo 0
}

txtmonth() {
  case $m_month in

  $NISSAN)
    echo "NISSAN"
    ;;
  $IYAR)
    echo "IYAR"
    ;;
  $SIVAN)
    echo "SIVAN"
    ;;
  $TAMUZ)
    echo "TAMUZ"
    ;;
  $AV)
    echo "AV"
    ;;
  $ELUL)
    echo "ELUL"
    ;;
  $TISHRI)
    echo "TISHRI"
    ;;
  $CHESHVAN)
    echo "CHESHVAN"
    ;;
  $KISLEV)
    echo "KISLEV"
    ;;
  $TEVET)
    echo "TEVET"
    ;;
  $SHVAT)
    echo "SHVAT"
    ;;
  $ADAR)
    if (($(isLeap))); then
      echo "ADAR1"
    else
      echo "ADAR"
    fi
    ;;
  $ADAR2)
    echo "ADAR2"
    ;;

  *)
    echo $m_month
    ;;
  esac
}
calculate() {
  local jdn=0
  local days=0
  local ml=0

  local uday=$((24 * 1080))
  local Tsyn=$((29 * uday + 12 * 1080 + 793))

  local hyear=$((1 + ((234 + 19 * (m_hdn + 1) * uday / Tsyn) / 235)))
  local rhnext=0
  local rh=$(st_roshHashanaDay $hyear)
  while ((rh > m_hdn)); do
    rhnext=$rh
    hyear=$((hyear - 1))
    rh=$(st_roshHashanaDay $hyear)
  done

  if ((rhnext == 0)); then
    rhnext=$(st_roshHashanaDay $(($hyear + 1)))
  fi
  m_type=$((((rhnext - rh) % 10) - 2))
  m_day=1
  m_month=$TISHRI
  m_year=$hyear
  days=$((m_hdn - rh))
  ml=$(getMonthLength)
  while ((days >= ml)); do

    local nbm=$(nbMonths)
    if (((m_month + 1) > nbm)); then
      m_month=1
    else
      m_month=$((m_month + 1))
    fi

    days=$((days - ml))
    ml=$(getMonthLength)
  done
  m_day=$((m_day + days))
}
setHdn() {
  m_hdn=$1
  if [ "$m_hdn" -lt "1" ]; then
    m_hdn=1
  fi
  calculate
}
setFromEpoch() {
  # 1-1-1970 = 2092591
  local epoch=$1
  setHdn $((2092591 + (epoch / (60 * 60 * 24))))
}
today() {
  setFromEpoch $(date +%s)
}

printHelp() {
   echo "jdate.sh print hebrew date or convert from hebrew date"
  echo "usage jdate.sh [[format] [UnixEpoch]] | [-c hebrewDay hebrewNumericMonth hebrewYear]"
  echo "format :"
  echo -e "\t-e [UnixEpoch]: Months in English (default)"
  echo -e "\t-n [UnixEpoch]: Months in numeric 1->13"
  echo "option :"
  echo -e "\t-c hebrewDay hebrewNumericMonth hebrewYear: Print the unix epoch and date month-day-year"
  echo -e "\t--chanukah: Chanukah Sameach"
  echo -e "\t--toda: Thanks Gab'"
}

chanuka(){
  today
  nbLight=$(( (m_day + (30*(m_month - KISLEV))) - 23 ))
  if (( nbLight > 0 && nbLight<=8 )) ; then
    echo "Happy Chanukah ! "
    echo "tonight we light up $nbLight Nerot (candles) plus the Shamash"
    local c="\033[1;38;2;255;255;0m|\033[m"
    echo -e "     $c"
    case $nbLight in
    1)
      echo -e "     Y    $c"
      ;;
    2)
      echo -e "     Y   $c$c"
      ;;
    3)
      echo -e "     Y  $c$c$c"
      ;;
    4)
      echo -e "     Y $c$c$c$c"
      ;;
    5)
      echo -e "   $c Y $c$c$c$c"
      ;;
    6)
      echo -e "  $c$c Y $c$c$c$c"
      ;;
    7)
      echo -e " $c$c$c Y $c$c$c$c"
      ;;
    8)
      echo -e "$c$c$c$c Y $c$c$c$c"
      ;;
    esac
    echo '\YYY | YYY/'
    echo ' \\\ | ///'
    echo '     | '
    echo '    / \ '
    else
      echo "Nothing to light tonight"
      echo "We are the $m_day/$(txtmonth)/$m_year "
      echo "Chanukah starts the 25 KISLEV"
      fi
}
case $1 in
"-h" | "--help")
  printHelp
  ;;
"-n")
  if [ "$2" == "" ]; then
    today
  else
    setFromEpoch $2
  fi
  echo $m_day $m_month $m_year
  ;;
"--chanukah")
   chanuka
   ;;
"-e")
  if [ "$2" == "" ]; then
    today
  else
    setFromEpoch $2
  fi
  echo $m_day/$(txtmonth)/$m_year
  ;;

"-c")
  if [ "$2" == "" ] ; then printHelp ; exit ;fi
  if [ "$3" == "" ] ; then printHelp ;exit ; fi
  if [ "$4" == "" ] ; then printHelp ;exit ; fi
  setDate_int_int_int $2 $3 $4
  epoch=$(( (m_hdn - 2092591 ) * 24 * 60 * 60 ))
  echo $epoch $(date -r $epoch +%m-%d-%Y )
  ;;
"--toda")
  echo "A huge thanks to calj.net where I took the all the javascript code !!! please visit and donate at https://calj.net"
;;
*)
  if [ "$1" == "" ]; then
    today
  else
    setFromEpoch $1
  fi
  echo $m_day/$(txtmonth)/$m_year
  ;;
esac


