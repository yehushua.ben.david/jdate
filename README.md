# Hebrew Calendar Bash Script

This script provides functionalities related to the Hebrew calendar.

## Features

1. Print the Hebrew date or convert from Hebrew date.
2. Translate Hebrew months to English or numeric format.
3. Display the current Unix Epoch date in Hebrew format.
4. Calculate the number of candles to light during Chanukah.
5. Convert a Hebrew date to Unix Epoch and display the corresponding Gregorian date.

## Usage

### Display Help
```bash
./jdate.sh -h
```

### Print Hebrew Date with Numeric Months
```bash
./jdate.sh -n [UnixEpoch]
```

### Celebrate Chanukah
```bash
./jdate.sh --chanukah
```

### Print Hebrew Date with English Months
```bash
./jdate.sh -e [UnixEpoch]
```

### Convert from Hebrew Date
```bash
./jdate.sh -c hebrewDay hebrewNumericMonth hebrewYear
```

### Display Appreciation Note
```bash
./jdate.sh --toda
```

### Default (Print Hebrew Date in English)
```bash
./jdate.sh [UnixEpoch]
```

## Acknowledgements

Special thanks to calj.net for the JavaScript code. Please consider visiting and donating at [calj.net](https://calj.net).
